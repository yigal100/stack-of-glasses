# Exercise: Stack of glasses

## Observation
According to the definition of the exercise each glass overflows evenly to both sides. 
This means that each interior glass (such as the glass at the middle of the third row - [2, 1]) receives half the overflow from the glass in its top-left position [1, 0] and half from its top-right position [1, 1].
In particular, each row requires knowledge on overflow from the previous row. 

Note: This is a variation on Pascal's triangle.

## Implementation
I've created a Glass class that handles puring of liquid into itself. It exposes the contents of the glass and the overflow for further calculations.
The GlassTriangle in turn generates alternating rows of glasses by "pouring" into each glass from the relevant two glasses from the previous row. 
We only keep two rows at any given time - the current and the previous per the observation above.

The requirement to illustrate was a bit unclear to me so I've added a verbose mode that would print the triangle of glasses as it is being calculated in addition to the final output.

in order to build:
> mvn clean install

In order to see usage:
> java -jar target/assignment-1.0-SNAPSHOT-jar-with-dependencies.jar 

With example parameters:
> java -jar target\assignment-1.0-SNAPSHOT-jar-with-dependencies.jar -r 3 -c 3 -l 2 