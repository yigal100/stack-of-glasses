package com.plexus;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

public class GlassTriangleTests {

    private boolean verbose = false;

    @Test
    public void calculate_negativeRow_throwsException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> GlassTriangle.calculate(-1, 2, 0, verbose));
        assertEquals("Row is negative", exception.getMessage());
    }

    @Test
    public void calculate_negativeColumn_throwsException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> GlassTriangle.calculate(2, -1, 0, verbose));
        assertEquals("Column is negative", exception.getMessage());
    }

    @Test
    public void calculate_columnOutsideTriangle_throwsException() {
        Exception exception = assertThrows(IndexOutOfBoundsException.class, () -> GlassTriangle.calculate(2, 3, 0, verbose));
        assertEquals("Column is outside triangle", exception.getMessage());
    }

    @Test
    public void calculate_negativeVolume_throwsException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> GlassTriangle.calculate(2, 2, -2.0, verbose));
        assertEquals("Volume is negative", exception.getMessage());
    }

    @DisplayName("calculate liquid in glass")
    @ParameterizedTest(name = "{index} ==> Glass[{0}, {1}], liquid={2}, contents={3}, overflow={4}")
    @CsvSource({
            "0, 0, 0.00, 0.00, 0.00", // empty case
            "0, 0, 0.25, 0.25, 0.00", // fill first glass
            "0, 0, 1.00, 0.25, 0.75", // fill first glass with overflow
            "1, 0, 0.75, 0.25, 0.0", // fill first glass on second row without overflow
            "1, 0, 1.00, 0.25, 0.125", // fill first glass on second row with overflow
            "2, 0, 1.00, 0.0625, 0.0", // overflow from previous test partially fills first glass on third row
            "2, 0, 2.0, 0.25, 0.0625", // fill first glass on third row with overflow
            "2, 1, 2.0, 0.25, 0.375", // ditto but verify middle glass was poured from both glasses on top
            "2, 2, 2.0, 0.25, 0.0625", // ditto for the other edge of the triangle
            "4, 2, 1.0, 0.0, 0.0", // glass is empty when there's insufficient liquid
    })
    public void calculate(int row, int column, double liquid, double expectedContents, double expectedOverflow) {
        Glass glass = GlassTriangle.calculate(row, column , liquid, verbose);
        assertAll(
                () -> assertEquals(expectedContents, glass.getContents()),
                () -> assertEquals(expectedOverflow, glass.getOverflow())
        );
    }
}
