package com.plexus;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GlassTests {

    @Test
    public void newGlassIsProperlyInitialized() {
        Glass glass = new Glass();
        assertAll(
                () -> assertEquals(0.250, glass.getCapacity()),
                () -> assertEquals(0.0, glass.getContents()),
                () -> assertEquals(0.0, glass.getOverflow())
        );
    }

    @Test
    public void pour_insufficientLiquid_glassPartiallyFilled() {
        Glass glass = new Glass();
        glass.pour(0.125);
        assertAll(
                () -> assertEquals(0.125, glass.getContents()),
                () ->assertEquals(0.0, glass.getOverflow())
        );
    }

    @Test
    public void pour_liquidToCapacity_glassFilledWithoutOverflow() {
        Glass glass = new Glass();
        glass.pour(glass.getCapacity());
        assertAll(
                () -> assertEquals(glass.getCapacity(), glass.getContents()),
                () ->assertEquals(0.0, glass.getOverflow())
        );
    }

    @Test
    public void pour_liquidAboveCapacity_glassFilledWithOverflow() {
        Glass glass = new Glass();
        glass.pour(glass.getCapacity() + 0.100);
        assertAll(
                () -> assertEquals(glass.getCapacity(), glass.getContents()),
                () ->assertEquals(0.100, glass.getOverflow(), 0.001)
        );
    }
}