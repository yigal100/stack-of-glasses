package com.plexus;

public class Glass {
    private final static double CAPACITY = 0.250;
    private double contents = 0.0;
    private double overflow = 0.0;

    @Override
    public String toString() {
        return "Glass{" +
                "contents=" + contents +
                ", overflow=" + overflow +
                '}';
    }

    public double getCapacity() {
        return CAPACITY;
    }

    public double getContents() {
        return contents;
    }

    public double getOverflow() {
        return overflow;
    }

    public void pour(double volume) {
        if (volume  > CAPACITY) {
            contents = CAPACITY;
            overflow = volume - CAPACITY;
        } else {
            contents = volume;
        }
    }
}
