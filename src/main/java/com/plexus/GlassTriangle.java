package com.plexus;

import snaq.util.jclap.CLAParser;
import snaq.util.jclap.OptionException;

public class GlassTriangle {

    public static void main(String[] args) {
        final CLAParser parser = new CLAParser();
        parser.addIntegerOption("r", "row", "Glass row", true, false);
        parser.addIntegerOption("c", "column", "Glass column", true, false);
        parser.addDoubleOption("l", "liquid", "Volume in litres of poured liquid", true, false);
        parser.addBooleanOption("v", "verbose", "Prints the the glass triangle during construction",false);
        try {
            parser.parse(args);
            int row = parser.getIntegerOptionValue("r");
            int column = parser.getIntegerOptionValue("c");
            double volume = parser.getDoubleOptionValue("l");
            boolean verbose = parser.getBooleanOptionValue("v", false);

            Glass glass = calculate(row, column, volume, verbose);

            System.out.printf("There is %fL of liquid in the [%d, %d] glass\n", glass.getContents(), row, column);
        } catch (OptionException e) {
            parser.printUsage(System.out, true);
        }
    }

    public static Glass calculate(int row, int column, double volume, boolean verbose) {
        if (row < 0) throw new IllegalArgumentException("Row is negative");
        if (column < 0) throw new IllegalArgumentException("Column is negative");
        if (volume < 0.0) throw new IllegalArgumentException("Volume is negative");
        if (column > row) throw new IndexOutOfBoundsException("Column is outside triangle");

        Glass[] current = new Glass[1];
        current[0] = new Glass();
        current[0].pour(volume);
        if (verbose) System.out.printf("[%s]\n", current[0]);

        for (int i = 1; i <= row; i++) {
            Glass[] next = new Glass[i + 1];
            next[0] = new Glass();
            next[0].pour(current[0].getOverflow() / 2);
            if (verbose) System.out.printf("[%s] ", next[0]);
            for (int j = 1; j < i; j++) {
                next[j] = new Glass();
                next[j].pour((current[j -  1].getOverflow() + current[j].getOverflow()) / 2);
                if (verbose) System.out.printf("[%s] ", next[j]);
            }
            next[i] = new Glass();
            next[i].pour(current[i - 1].getOverflow() / 2);
            if (verbose) System.out.printf("[%s]\n", next[i]);
            current = next;
        }
        return current[column];
    }
}
